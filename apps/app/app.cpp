#include "panda_control_dense.h"
#include <thread>
#include <signal.h>
#include <iostream>
#include <array>
#include <algorithm>

volatile int keepRunning = 1;

void intHandler(int dummy) {
    keepRunning = 0;
}

int main(){
  signal(SIGINT, intHandler);
  signal(SIGHUP, intHandler);

  while(keepRunning){
    
    pandev::visualizer v;
    Eigen::VectorXd q(7);
    q = pandev::q_0_v;
    int i {0};

    std::array<double,16> ar = {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6};
    Eigen::Affine3d T;
    T.matrix()  = Eigen::Map<Eigen::Matrix<double,4,4> >(ar.data());

    Eigen::Vector3d a,b,c;

    a << 1,2,3;
    b << 4,4,5;
    c << 2,1,1;

    std::cout << (a.array()<4).all() << std::endl;
    std::cout << (a.array()>4).any() << std::endl;
    std::cout << (a.array()<c.array()).all() << std::endl;
    std::cout << (a.array()<c.array()).any() << std::endl;

    std::cout << T.matrix() << std::endl;

    std::array<double,3> AR;

    std::copy(std::begin(a.array()), std::end(a.array()), AR.begin());

    std::cout << AR[0] << AR[1] << AR[2] << std::endl;


    while (true && keepRunning){
      q(3) = pandev::q_0_v(3)+sin(i*0.1);
      i++;
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      v.setPositions(q);
    }
  }
  return 0;
}
