#include "panda_control_dense.h"
#include <iostream>
#include <thread>
#include <signal.h>
#include <chrono>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>

volatile int keepRunning = 1;

void intHandler(int dummy) {
  keepRunning = 0;
}

using namespace pandev;
using namespace Eigen;
using namespace std;


int main(int argc, char** argv) {

  int systemRet = std::system("/home/joao/packages/space_mouse/spnav/spacenavd/spacenavd");
  try{
    systemRet = std::system("sudo chmod a+rw /dev/ttyACM0");                    // distance sensor
    systemRet = std::system("sudo chmod a+rw /dev/ttyACM1");                    // print head
    systemRet = std::system("clear");
  }
  catch(...){
    std::cout << "Serial ports could not be accessed\n";
  }
  signal(SIGINT, intHandler);
  signal(SIGHUP, intHandler);

  pandev::DistanceSensor    dst   { keepRunning                 };
  
  pandev::output_mode om                                        ;
  std::string         ip_addres                                 ;
  pandev::visualizer  v                                         ;
  pandev::Set_Output_Mode(om,ip_addres,argc,argv)               ; 
  pandev::controller  cnt         { ip_addres, om, keepRunning  };  
  const double        dt          {   1e-3                      };
  if (om == pandev::experimental){
    pandev::initialize(*(cnt.rb_));
  }
  const double            k_sm    { 2.0e-5                      };
  double                  time    { 0                           };        
  double                  omega   { 0                           };  
  double                  distance{ 0                           };      
  pandev::TimeTrack       tt      { dt                          };
  Eigen::Vector3d         u       { Eigen::Vector3d::Zero()     };
  Eigen::Vector4d         vplot   { Eigen::Vector4d::Zero()     };
  Eigen::Vector7d         q       { pandev::q_0_v               };
  Eigen::Matrix67d        J       { Eigen::Matrix67d::Zero()    };
  Eigen::Vector7d         Dq      { Eigen::Vector7d::Zero()     };
  Eigen::Vector6d         Dxd     { Eigen::Vector6d::Zero()     };
  Eigen::Vector6d         sm_m    { Eigen::Vector6d::Zero()     };
  Eigen::Affine3d         xd      { Eigen::Affine3d::Identity() };    
  std::vector<int>        bsm     ( 2,  0                       );
  pandev::model           md                                    ;    
  pandev::US_OUT          us_out                                ;
  pandev::OUT_US          out_us                                ;
  spc_ms::spc_ms          sm      { std::vector<int>({2,4})     };   
  pandev::print_stt_dir   psd     { pandev::print_stt_dir::psd_idle };

  pandev::Plotter         plt(4,{"d","xd","yd","zd"},keepRunning);

  /////////////////////////////////////////////////////////////////////////////////
  // see notes 10.05.22
  const double            vabs    { 4e-3                      };
  const double            stp     { vabs * dt                 };
  const double            l1      { 10e-3                     };
  const double            l2      { 10e-3                     };
  const double            s       { 12e-4                     };
  const double            sr      { s/2.0                     };
  const double            h       { 8e-4                      };  
  bool                    vdir    { true                      };
  Eigen::Vector2d         vmv     { Eigen::Vector2d::Zero()   };
  double                  x0      { 0                         };
  double                  y0      { 0                         };
  double                  z0      { 0                         };
  double                  xdp     { 0                         };
  double                  ydp     { 0                         };  
  double                  zdp     { 0                         };
  double                  xf      { 0                         };
  double                  yf      { 0                         };  
  double                  xc      { 0                         };  
  double                  yc      { 0                         };
  double                  xr1     { 0                         };
  double                  xr2     { 0                         };    
  double                  theta   { 0                         };  
  double                  dtheta  { vabs/sr*dt                };
  double                  t2h     { 0                         };  
  int                     layer   { 0                         };
  bool                    t2hset  { false                     };
  bool                    corner  { false                     };
  bool                    move    { false                     };
  bool                    frst_rw { false                     };  
  /////////////////////////////////////////////////////////////////////////////////

  md.KinMod(xd,J,q);
  if (om == pandev::experimental){
    cnt.LauchMainControl(keepRunning);     
  }
  tt.launch_track();
  us_out.cm               = pandev::cartesian_position;
  cnt.phc.Enable();  
  bool          zset      {false};
  double        zr        {0}   ;
  double        k         {dt*1.3e1};  
  while(keepRunning){ 
    sm.read(sm_m,bsm);
    time                  = tt.time_span();
    psd                   = pandev::print_stt_dir::psd_idle;
    Dxd.setZero();
    distance              = dst.Read();   
    if (bsm.at(0) == 1){
      if (bsm.at(1) == 1){        
        if (!zset){
          zr              = distance + h*1e3;
          zset            = true;
        }        
      }      
      else if(bsm.at(1) == 2){
        if (!t2hset){
          t2h             = time;
          t2hset          = true;
        }
        Dxd(2,0)          = k*(zr + layer*h*1e3 - distance);
        xd.translation()  = xd.translation() + Dxd.topRows(3)*dt;
        x0                = out_us.x.translation()(0);
        y0                = out_us.x.translation()(1);        
        z0                = out_us.x.translation()(2);        
        xc                = x0;
        yc                = y0;
        xr1               = x0 + s;
        xr2               = x0 + l1 - s;  
        zdp               = xd.translation()(2);
        if (time > t2h + 1.0){
          sm.set_states(1,3);
          t2hset          = false;
        }
      }
      else if(bsm.at(1) == 3){        
        xdp               = xd.translation()(0);
        ydp               = xd.translation()(1);
        zdp               = xd.translation()(2);
        if (move){
          if (zdp < z0 + 1.5*h ){
            zdp           += vabs*dt;
          }
          else if (ydp > y0){
            vmv           << x0-xf, y0-yf;
            vmv.normalize();
            xdp           += vabs*dt*vmv(0);
            ydp           += vabs*dt*vmv(1);
          }else {
            xdp           = x0;
            ydp           = y0;
            sm.set_states(1,2);
            move          = false;
            corner        = false;
            vdir          = true;
          }
        } else if (corner){
          theta           += dtheta;
          if (theta > M_PI){
            if (xdp < xr1){
              xdp         = xr1;
            }else{
              xdp         = xr2;
            }                      
            ydp           = yc + s;
            corner        = false;            
            vdir          = !vdir;
          }
          else{
            if (xdp < xr1){
              xdp         = xr1 - sr*sin(theta);
              ydp         = yc + sr*(1-cos(theta));            
            }else{
              xdp         = xr2 + sr*sin(theta);
              ydp         = yc + sr*(1-cos(theta));
            }          
          }
          psd             = pandev::print_stt_dir::psd_print;
        }
        else{
          xdp             += (vdir) ? vabs*dt : -vabs*dt;
          if ((xdp < xr1 && frst_rw) || ((xdp > xr2 ))){
            frst_rw       = true;
            corner        = true;
            yc            = ydp;
            theta         = 0;
            if (yc > y0 + l2){
              move        = true;
              frst_rw     = false;
              corner      = false;
              xf          = xdp;
              yf          = ydp;
              layer++;
            }                   
          }
          psd             = pandev::print_stt_dir::psd_print;            
        }        
        xd.translation()  << xdp, ydp, zdp;
      }
      else{        
        zset              = false;
        Dxd               = sm_m * k_sm;
        xd.translation()  = xd.translation() + Dxd.topRows(3)*dt;
        xd.linear()       = Eigen::AngleAxisd( Dxd.norm()*dt, Dxd.bottomRows(3).normalized()).matrix() * xd.linear();        
        Dxd.setZero();
      }
    }

    us_out.time           = time;                                         // attribute time, q and xd to structure d
    us_out.xd             = xd;
    us_out.Dxd            = Dxd;
    us_out.psd            = psd;
    cnt.set_us_out(us_out);
    cnt.OuterControl();
    cnt.get_out_us(out_us);
    
    if(om == pandev::simulation){
      Dq                = cnt.InnerControl();        
      q                 = q + dt*Dq;
      v.setPositions(q);                                          
      cnt.set_q(q);
    }    
    vplot               << distance, xd.translation();
    plt.update(vplot.array().data());
    tt.sleep();                                                 
  }
}